# Kubesec Básico

O Kubesec é uma ferramenta que permite avaliar a segurança de arquivos de manifesto do Kubernetes. Ele verifica os arquivos YAML em busca de problemas de segurança e fornece recomendações sobre como corrigi-los. Siga as etapas abaixo para começar a usar o Kubesec:

Passo 1: Instale o Kubesec
Você pode instalar o Kubesec executando o seguinte comando em seu terminal:

```
curl -SLfs https://github.com/controlplaneio/kubesec/releases/download/v2.11.0/kubesec_linux_amd64.tar.gz | tar -xzC /tmp && sudo mv /tmp/kubesec /usr/local/bin/
```

Certifique-se de que o `curl` e o `tar` estejam instalados em seu sistema antes de executar este comando.

Passo 2: Verifique um arquivo de manifesto
Após instalar o Kubesec, você pode verificar um arquivo de manifesto do Kubernetes executando o seguinte comando:

```
kubesec <caminho_para_o_arquivo_manifesto>
```

Substitua `<caminho_para_o_arquivo_manifesto>` pelo caminho para o arquivo YAML que você deseja verificar. Por exemplo:

```
kubesec ./meu_arquivo.yaml
```

O Kubesec analisará o arquivo e fornecerá uma pontuação de segurança com base nas melhores práticas do Kubernetes. Ele também indicará quaisquer problemas de segurança encontrados e fornecerá recomendações sobre como corrigi-los.

Passo 3: Interpretar os resultados
Após a verificação do arquivo de manifesto, o Kubesec exibirá os resultados na saída do terminal. Ele fornecerá uma pontuação de segurança de 0 a 10, sendo 10 a pontuação máxima. Quanto mais próximo de 10, melhor é a segurança do arquivo.

O Kubesec também mostrará uma lista de problemas de segurança encontrados no arquivo de manifesto. Cada problema será acompanhado por uma descrição e uma recomendação sobre como corrigi-lo.

Passo 4: Corrigir os problemas de segurança
Com base nas recomendações fornecidas pelo Kubesec, você pode corrigir os problemas de segurança em seu arquivo de manifesto. Edite o arquivo YAML e faça as alterações necessárias para resolver os problemas identificados.

Depois de fazer as alterações, execute novamente o comando `kubesec` no arquivo atualizado para verificar se os problemas de segurança foram resolvidos.